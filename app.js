(function() {
    var app = angular.module('store', [ ]);
    app.controller('StoreController', function() {
	this.products = gems;
    });
    app.controller('PanelController', function() {
	this.tab = 1; 
	this.selectTab = function(setTab) {
	    this.tab = setTab;
	};
	this.isSelected = function(checkTab) {
	    return this.tab === checkTab;
	};
    });
    var gems = [
    {
	name: 'Dodecahedron',
	price: 2,
	description: 'This is a rare gem found in the land of Mororo .. it\'s very shiny and beyond description',
	canPurchase: true,
	soldOut: true,
	image: 'imgs/gem1.jpg',
    },
    {
	name: 'Pentagonal Gem',
	price: 5.32,
	description: 'Here\'s another cute Gem, it\'s red and awesome',
	canPurchase: true,
	soldOut: false,
	image: 'imgs/gem2.jpg',
    },
    ];
})();
